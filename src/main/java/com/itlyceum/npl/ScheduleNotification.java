package com.itlyceum.npl;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.itlyceum.npl.ui.HomeActivity;

/**
 * Author: Gulnaz Sibgatullina
 * Date: 29.01.14
 * Time: 12:33
 */
public class ScheduleNotification extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String drug = intent.getStringExtra("drug");
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(R.drawable.ic_launcher, String.format("Необходимо принять %s", drug), System.currentTimeMillis());
        //Интент для активити, которую мы хотим запускать при нажатии на уведомление
        Intent intentTL = new Intent(context, HomeActivity.class);
        notification.setLatestEventInfo(context, "ПЛЛ", String.format("Необходимо принять %s", drug),
                PendingIntent.getActivity(context, 0, intentTL,
                        PendingIntent.FLAG_CANCEL_CURRENT));
        notification.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL;
        nm.notify(1, notification);
        /*// Установим следующее напоминание.
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);
        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + AlarmManager.INTERVAL_DAY, pendingIntent);*/
    }
}
package com.itlyceum.npl.model;

import java.util.*;

/**
 * Author: Gulnaz Sibgatullina
 * Date: 26.01.14.
 */
public class Schedule {
    private String title;
    private Drug drug;
    private Date startDate;
    private List<String> takingTimeList;
    private List<Integer> dayOfWeeks = new ArrayList<Integer>();
    private Repeat repeat;
    private String description;
    private String drugName;

    public List<String> getTakingTimeList() {
        return takingTimeList;
    }

    public void setTakingTimeList(List<String> takingTimeList) {
        this.takingTimeList = takingTimeList;
    }

    public int getId(){
        return (title.hashCode() + getDrugName().hashCode());
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
      this.title = title;
    }

    public Drug getDrug() {
        return drug;
    }

    public void setDrug(Drug drug) {
        this.drug = drug;
    }

    public String getDrugName(){
        if (drug==null) {
            return drugName;
        } else {
            return drug.getTitle();
        }
    }

    public void setDrugName(String drugName){this.drugName = drugName;}


    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Repeat getRepeat() {
        return repeat;
    }

    public void setRepeat(Repeat repeat) {
        this.repeat = repeat;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Integer> getDayOfWeeks() {
        return dayOfWeeks;
    }

    public void setDayOfWeeks(List<Integer> dayOfWeeks) {
        this.dayOfWeeks = dayOfWeeks;
    }

    public enum Repeat{
        Hour,
        Day,
        Week,
        Month;

        private Date endDate;

        public void setEndDate(Date date){
            endDate = date;
        }
    }

    public Date getNextNotifyDate() {
        Date nextDate = calculateNearestDate();
        if(nextDate != null) {
            Calendar nextDateCalendar = Calendar.getInstance();
            nextDateCalendar.setTime(nextDate);

            try {
                int[] hourMin = calculateNearestHourAndMinute();
                nextDateCalendar.set(Calendar.HOUR_OF_DAY, hourMin[0]);
                nextDateCalendar.set(Calendar.MINUTE, hourMin[1]);

                return nextDateCalendar.getTime();
            } catch (RuntimeException rex) {
                return null;
            }

        } else {
            return null;
        }
    }

    /**
     * Рассчитывает ближайшую дату приема
     * @return
     */
    protected Date calculateNearestDate() {
        Calendar currentDate = Calendar.getInstance();
        Calendar startDate   = Calendar.getInstance();
        startDate.setTime(this.startDate);

        //если текущее время меньше начала приема, то возвращаем сразу ее
        if(currentDate.getTimeInMillis() < startDate.getTimeInMillis()) {
            return this.startDate;
        } else {
            //получаем текущий день недели
            int currentDayOfWeek = currentDate.get(Calendar.DAY_OF_WEEK);

            //если текущий день недели есть в массиве выбранных дней приема
            if(dayOfWeeks.contains(currentDayOfWeek)) {
                return currentDate.getTime();
            } else {
                for(int i =0; i < 7; i++) {
                    currentDate.add(Calendar.DATE, 1);
                    if(dayOfWeeks.contains(currentDate.get(Calendar.DAY_OF_WEEK))) {
                        return currentDate.getTime();
                    }
                }

                return null;
            }
        }
    }

    protected int[] calculateNearestHourAndMinute() {
        Calendar currentDay = Calendar.getInstance();
        List<Date> futureTimes = new ArrayList<Date>();
        Calendar date = Calendar.getInstance();
        for(String time : takingTimeList) {
            String[] hourTime = time.split(":");
            date.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hourTime[0]));
            date.set(Calendar.MINUTE, Integer.parseInt(hourTime[1]));

            long mills = date.getTimeInMillis();

            if((mills - currentDay.getTimeInMillis()) >= 0) {
                futureTimes.add(date.getTime());
            }
        }

        Collections.sort(futureTimes);

        Calendar nearestTime = Calendar.getInstance();

        nearestTime.setTime(futureTimes.get(0));

        return new int[] {nearestTime.get(Calendar.HOUR_OF_DAY), nearestTime.get(Calendar.MINUTE)};
    }
}

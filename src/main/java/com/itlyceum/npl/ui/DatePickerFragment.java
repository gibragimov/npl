package com.itlyceum.npl.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockDialogFragment;
import com.itlyceum.npl.model.Drug;
import com.itlyceum.npl.storage.DrugStorageInterface;

import java.util.Calendar;

/**
 * Author: Gulnaz Sibgatullina
 * Date: 27.01.14.
 */
public class DatePickerFragment extends RoboSherlockDialogFragment implements DatePickerDialog.OnDateSetListener {
    protected DatePickerDialog.OnDateSetListener listener;

    public void setOnDateSetListener(DatePickerDialog.OnDateSetListener listener) {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Calendar calendar = Calendar.getInstance();

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);

        return new DatePickerDialog(getActivity(), listener, year,month,day);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i2, int i3) {

    }
}

package com.itlyceum.npl.ui;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.*;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;
import com.google.inject.Inject;
import com.itlyceum.npl.R;
import com.itlyceum.npl.ScheduleNotification;
import com.itlyceum.npl.injection.Utils;
import com.itlyceum.npl.model.Schedule;
import com.itlyceum.npl.storage.ScheduleStorageInterface;
import roboguice.inject.InjectView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Author: Gulnaz Sibgatullina
 * Date: 26.01.14.
 */
public class AddScheduleActivity extends RoboSherlockFragmentActivity {
    @Inject
    protected ScheduleStorageInterface scheduleStorage;

    @InjectView(R.id.schedule_title)
    protected EditText titleView;

    @InjectView(R.id.schedule_drug)
    protected EditText drugNameView;

    @InjectView(R.id.show_date_picker)
    protected Button showDatePickerBtn;

    @InjectView(R.id.add_taking_time)
    protected Button addTakingTimeBtn;

    @InjectView(R.id.setup_schedule_repeat)
    protected Button setupRepeatBtn;

    @InjectView(R.id.schedule_description)
    protected EditText scheduleDescriptionView;

    @InjectView(R.id.taking_time_list_view)
    protected ListView takingTimeListView;

    protected List<String> takingTimeList = new ArrayList<String>();

    protected ArrayAdapter<String> takingListAdapter;

    protected Date selectedDate = new Date();
    protected List<Integer> selectedDays = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_schedule);
        getSupportActionBar().setTitle(R.string.add_schedule_activity_title);
        takingListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, takingTimeList);
        showDatePickerBtn.setText(String.format("%te %<ta %<tR", selectedDate));
        showDatePickerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment datePickerFragment = new DatePickerFragment();
                datePickerFragment.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        selectedDate = calendar.getTime();
                        showDatePickerBtn.setText(String.format("%te %<ta %<tR", selectedDate));
                    }
                });
                datePickerFragment.show(getSupportFragmentManager(), "date_fragment");
            }
        });

        addTakingTimeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerFragment timePickerFragment = new TimePickerFragment();
                timePickerFragment.setTimeSetListener(new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        takingTimeList.add(String.format("%s:%s", hourOfDay, minute));
                        takingListAdapter.notifyDataSetChanged();
                        Utils.setListViewHeightBasedOnChildren(takingTimeListView);
                    }
                });
                timePickerFragment.show(getSupportFragmentManager(), "time_fragment");
            }
        });
        setupRepeatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectDayFragment selectDayFragment = new SelectDayFragment();
                selectDayFragment.setListener(new SelectDayFragment.OnSelectedDaysListener() {
                    @Override
                    public void onSelectedDays(List<Integer> days) {
                        selectedDays = days;
                    }
                });
                selectDayFragment.show(getSupportFragmentManager(),"day_fragment");
            }
        });
        takingTimeListView.setAdapter (takingListAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.activity_add_schedule, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_add:
                if(validateForm()) {
                    saveForm();
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean validateForm() {
        String error = "";

        if(TextUtils.isEmpty(titleView.getText().toString())) {
            error = "Необходимо указать название";
        } else if(TextUtils.isEmpty(drugNameView.getText().toString())) {
            error = "Необходимо указать название лекарства";
        } else if(selectedDays.isEmpty()) {
            error = "Необходимо выбрать дни приема";
        }

        if(!TextUtils.isEmpty(error)) {
            new AlertDialog.Builder(this)
                    .setMessage(error)
                    .setPositiveButton(android.R.string.ok, null)
                    .create()
                    .show();

            //Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    public void saveForm() {
        Schedule schedule1 = new Schedule();

        schedule1.setTitle(titleView.getText().toString());
        schedule1.setDrugName(drugNameView.getText().toString());
        schedule1.setDayOfWeeks(selectedDays);
        schedule1.setStartDate(selectedDate);
        schedule1.setDescription(scheduleDescriptionView.getText().toString());
        schedule1.setTakingTimeList(takingTimeList);

        scheduleStorage.save(schedule1);

        restartNotify(schedule1);

        finish();
    }

    private void restartNotify(Schedule schedule) {
        Date dt = schedule.getNextNotifyDate();
        if(dt != null) {
            AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(this, ScheduleNotification.class);
            intent.putExtra("drug", schedule.getDrugName());
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT );
            // На случай, если мы ранее запускали активити, а потом поменяли время,
            // откажемся от уведомления
            am.cancel(pendingIntent);
            // Устанавливаем разовое напоминание
            am.set(AlarmManager.RTC_WAKEUP, schedule.getNextNotifyDate().getTime(), pendingIntent);
        }
    }
}

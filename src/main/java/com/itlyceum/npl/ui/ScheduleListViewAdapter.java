package com.itlyceum.npl.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.itlyceum.npl.R;
import com.itlyceum.npl.model.Schedule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Author: Gulnaz Sibgatullina
 * Date: 26.01.14.
 */
public class ScheduleListViewAdapter extends BaseAdapter{
    protected List<Schedule> scheduleList;
    protected LayoutInflater inflater;

    public ScheduleListViewAdapter(Context context, Schedule[] schedules) {
        scheduleList = new ArrayList<Schedule>(Arrays.asList(schedules));
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return scheduleList.size();
    }

    @Override
    public Object getItem(int i) {
        return scheduleList.get(i);
}

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void remove(Schedule schedule) {
        scheduleList.remove(schedule);
    }

    public void remove(int position) {
        scheduleList.remove(position);
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        Schedule schedule = scheduleList.get(position);
        view = inflater.inflate(R.layout.lv_schedule_item, null);
        ((TextView)view.findViewById(R.id.schedule_title)).setText(schedule.getTitle());
        ((TextView)view.findViewById(R.id.schedule_drug)).setText(schedule.getDrugName());
        ((TextView)view.findViewById(R.id.schedule_time)).setText(String.format("%te %<ta %<tR", schedule.getNextNotifyDate()));
        ((TextView)view.findViewById(R.id.schedule_description)).setText(schedule.getDescription());


        return view;
    }
}

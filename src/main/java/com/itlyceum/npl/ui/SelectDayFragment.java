package com.itlyceum.npl.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.itlyceum.npl.R;

import java.util.*;

/**
 * Author: Gulnaz Sibgatullina
 * Date: 27.01.14.
 */
public class SelectDayFragment extends DialogFragment implements DialogInterface.OnMultiChoiceClickListener {
    List<String> dayList = Arrays.asList(/*"Каждый день",*/"Воскресенье", "Понедельник", "Вторник","Среда","Четверг","Пятница","Суббота");
    protected boolean[] selectedItems;
    protected OnSelectedDaysListener listener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        selectedItems = new boolean[dayList.size()];
        for(int i = 0; i < selectedItems.length; i++)
            selectedItems[i] = false;
    }

    /*@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dialog_select_day,null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ListView listView = (ListView)view.findViewById(R.id.days_list_view);
        listView.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_multichoice, dayList));
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);


    }*/

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle("Дни приема")
                .setMultiChoiceItems(dayList.toArray(new CharSequence[dayList.size()]), selectedItems, this)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        List<Integer> selectedDays = new ArrayList<Integer>();
                        for (int j = 0; j < dayList.size(); j++) {
                            if (selectedItems[j]) {
                                selectedDays.add(j+1);
                            }
                        }

                        if (listener != null) {
                            listener.onSelectedDays(selectedDays);
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
        selectedItems[which] = isChecked;
    }

    public OnSelectedDaysListener getListener() {
        return listener;
    }

    public void setListener(OnSelectedDaysListener listener) {
        this.listener = listener;
    }

    public interface OnSelectedDaysListener {
        public void onSelectedDays(List<Integer> days);
    }
}

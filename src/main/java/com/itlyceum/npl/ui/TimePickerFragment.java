package com.itlyceum.npl.ui;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.TimePicker;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockDialogFragment;

import java.util.Calendar;

/**
 * Author: Gulnaz Sibgatullina
 * Date: 27.01.14.
 */
public class TimePickerFragment extends RoboSherlockDialogFragment {
    protected TimePickerDialog.OnTimeSetListener timeSetListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        return new TimePickerDialog(getActivity(), timeSetListener, hour,minute,true );
    }

    public void setTimeSetListener(TimePickerDialog.OnTimeSetListener timeSetListener) {
        this.timeSetListener = timeSetListener;
    }




}
